# Use the official Nginx image as the base image
FROM nginx

# Copy custom configuration file from the current directory to the container
#COPY nginx.conf /etc/nginx/nginx.conf

# Expose port 80 to allow outside access
EXPOSE 80

# Command to start Nginx when the container starts
CMD ["nginx", "-g", "daemon off;"]
