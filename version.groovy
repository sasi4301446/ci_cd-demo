// version.groovy
import java.io.File

def getVersion() {
    def versionFile = new File('/home/ci/version.txt')
    def currentVersion = 0
    if (versionFile.exists()) {
        currentVersion = versionFile.text.toInteger()
    } else {
        versionFile.write('1')
    }
    def nextVersion = currentVersion + 1
    versionFile.text = nextVersion.toString()
    return "v$nextVersion"
}

